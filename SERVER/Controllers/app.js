﻿module.exports = { main, query };

const axios = require('axios');

const config = require('./../config.json');

function main(req, res) {
    var html = require(`fs`).readFileSync(`${__dirname}/../misc/map.html`, 'utf8');

    res.setHeader('content-type', 'text/html');
    res.end(html);
}

function query(req, res) {
    var savedUpperLeft = req.body.savedUpperLeft;
    var savedLowerRight = req.body.savedLowerRight;
    var dividerCount = req.body.dividerCount;

    var horizontalLength = savedUpperLeft.lng - savedLowerRight.lng;
    var horizontalDivider = horizontalLength / (dividerCount - 1);
    var verticalLength = savedUpperLeft.lat - savedLowerRight.lat;
    var verticalDivider = verticalLength / (dividerCount - 1);

    var params = [];
    var responses = [];

    // Uncomment for testing
    //queryEnd(res, responses, savedUpperLeft, savedLowerRight); return;

    // Preparing parameters for here.com API
    for (var j = 0; j < dividerCount; j++) {
        var lat = savedUpperLeft.lat - j * verticalDivider;

        for (var i = 0; i < dividerCount; i++) {
            var lng = savedUpperLeft.lng - i * horizontalDivider;

            var index = j * dividerCount + i;
            if (index % 100 == 0)
                params.push("");

            params[params.length - 1] += `id=${index}&prox=${lat},${lng},50\n`;
        }
    }

    var appID = config.here_api_app_id;
    var appCODE = config.here_api_app_code;
    var url = `https://reverse.geocoder.api.here.com/6.2/multi-reversegeocode.json?mode=retrieveAddresses&app_id=${appID}&app_code=${appCODE}`;

    for (var i = 0; i < params.length; i++) {
        axios
            .post(url, params[i], { headers: { 'Content-Type': '*' } })
            .then(function (response) {
                responses.push(response.data.Response.Item);

                // Collecting responses from API calls. Once each API calls finished...
                if (responses.length == params.length)
                    queryEnd(res, responses, savedUpperLeft, savedLowerRight); //... call queryEnd to merge and return API calls' results
            })
            .catch(function (err) { console.log(err); })
    }
}

function queryEnd(res, responses, savedUpperLeft, savedLowerRight) {
    var addresses = [];
    var addressesCheck = [];

    /////////////////////////
    // Uncomment for testing
    /*
    var resultArr = [];
    resultArr.push({ "Location": { "Address": { "Label": "4814 Hopkins Ave, Dallas, TX 75209, USA" }, "DisplayPosition": { "Latitude": 32.847491, "Longitude": -96.828076 } } });
    resultArr.push({ "Location": { "Address": { "Label": "4903 Wateka Dr, Dallas, TX 75209, USA" }, "DisplayPosition": { "Latitude": 32.844324, "Longitude": -96.827249 } } });

    var currItem = { Result: resultArr };
    var innerResponse = [];
    innerResponse.push(currItem);
    responses = [];
    responses.push(innerResponse);
    */
    /////////////////////////

    for (var i = 0; i < responses.length; i++) {
        var currResponse = responses[i];

        for (var k = 0; k < currResponse.length; k++) {
            var currItem = currResponse[k];

            for (var j = 0; j < currItem.Result.length; j++) {
                var currResult = currItem.Result[j];

                var validAddress = (currResult.Location.Address.Street != undefined) && (currResult.Location.Address.HouseNumber != undefined);
                var uniqueAddress = addressesCheck.indexOf(currResult.Location.Address.Label) == -1

                if (validAddress)
                    if (uniqueAddress) {
                        var address = currResult.Location.Address.Label;
                        var currCoords = currResult.Location.DisplayPosition;

                        var within1 = (currCoords.Latitude <= savedUpperLeft.lat) && (currCoords.Latitude >= savedLowerRight.lat);
                        var within2 = (currCoords.Longitude >= savedUpperLeft.lng) && (currCoords.Longitude <= savedLowerRight.lng);
                        
                        var out = "out ";
                        if (within1 && within2) {
                            out = "";

                            addresses.push({
                                currCoords: currCoords,
                                address: `${out}${currResult.Location.Address.Country} ${currResult.Location.Address.County} ${currResult.Location.Address.City} ${currResult.Location.Address.PostalCode} ${currResult.Location.Address.Street} ${currResult.Location.Address.HouseNumber}`
                            });
                            addressesCheck.push(address);
                        }
                    }
            }
        }
    }

    addresses.sort(function (a, b) { return (a.address > b.address) ? 1 : ((b.address > a.address) ? -1 : 0); });

    res.setHeader('content-type', 'application/json');
    res.end(JSON.stringify(addresses));
}