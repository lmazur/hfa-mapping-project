mapboxgl.accessToken = 'pk.eyJ1IjoiaGZhIiwiYSI6ImNqdDNwZDd3NjB2NWk0M2xsNG4zb2k3MWIifQ.cUDV67Mj0UuiuOZfvpvVLA';

var geocoder = new MapboxGeocoder({accessToken: mapboxgl.accessToken});
var navcontrol = new mapboxgl.NavigationControl();

var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: [-96.828, 32.844],
    zoom: 16
});
map.addControl(geocoder);
map.addControl(navcontrol);

map.on('mousemove', function (e) {
    document.getElementById('info').innerHTML = "Long = " + e.lngLat.lng + ", Lat = " + e.lngLat.lat;
});

map.on('click', function (e) {
    mapClick(e);
});

map.on('load', function () {

    map.addSource("zones", {
        "type": "vector",
        "url": "mapbox://hfa.9ncc6dhu"
    });

    map.addLayer({
        "id": "zones-layer",
        "type": "fill",
        "source": "zones",
        "source-layer": "zones",
        "layout": {
            "visibility": "visible"
        },
        "paint": {
            'fill-color': 'rgba(200, 100, 240, 0.4)',
            'fill-outline-color': 'rgba(200, 100, 240, 1)',
            "fill-opacity": 0.8
        }
    });
});

(function addToggle() {
    let link = document.createElement("a");
    link.href = "#";
    link.className = "active";
    link.textContent = "Zones";

    link.onclick = function (e) {

        e.preventDefault();
        e.stopPropagation();

        let visibility = map.getLayoutProperty("zones-layer", "visibility");

        if (visibility === "visible") {
            map.setLayoutProperty("zones-layer", "visibility", "none");
            this.className = "";
        } else {
            this.className = "active";
            map.setLayoutProperty("zones-layer", "visibility", "visible");
        }
    };

    let layers = document.getElementById("menu");
    layers.appendChild(link);
})();

var savedUpperLeft, savedLowerRight;
var dividerCount;

var savedUpperLeftTmp = {"lng": -96.83091824340643, "lat": 32.84612273285059};
var savedLowerRightTmp = {"lng": -96.82482426452326, "lat": 32.84210256736081}

//mapClick({ "lngLat": savedUpperLeftTmp }); mapClick({ "lngLat": savedLowerRightTmp });

function mapClick(e) {
    if ((savedLowerRight != undefined) && (savedLowerRight != undefined)) {
        alert('Please reload the page to select new vertexinates!');
        return;
    }
    else {
        addMarker(e.lngLat.lng, e.lngLat.lat, 'marker');

        if ($('#savedUpperLeft').html() == '') {
            savedUpperLeft = e.lngLat;
            $('#savedUpperLeft').html("Long = " + e.lngLat.lng + ",<br/> Lat = " + e.lngLat.lat);
        }
        else {
            savedLowerRight = e.lngLat;
            $('#savedLowerRight').html("Long = " + e.lngLat.lng + ",<br/> Lat = " + e.lngLat.lat);

            dividerCount = 10;
            drawRectangle();

            //dividerCount = 10;
            //APIcall({ savedUpperLeft: savedUpperLeft, savedLowerRight: savedLowerRight, dividerCount: dividerCount }, '10x10_Grid');
            dividerCount = 100;
            APIcall({
                savedUpperLeft: savedUpperLeft,
                savedLowerRight: savedLowerRight,
                dividerCount: dividerCount
            }, '100x100_Grid');
        }
    }
}

function APIcall(coors, divName) {
    $("#process").html('Loading by ' + divName);

    $.ajax({
        url: '/query',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(coors),
        success: function (json) {
            $("#process").html("Number of Results: " + json.length);
            var html = "";

            for (var i = 0; i < json.length; i++) {
                var currItem = json[i];
                var currCoords = currItem.currCoords;
                html += "[" + i + ".] <a href='#' onclick='addMarker(" + currCoords.Longitude + "," + currCoords.Latitude + ", \"markerSelected\"); return false;'>" + currItem.address + "</a><br/>";
            }

            $("#" + divName).html(html);
        }
    })
}

function drawRectangle() {

    let rectangle = [
        [savedUpperLeft.lng, savedUpperLeft.lat],
        [savedLowerRight.lng, savedUpperLeft.lat],
        [savedLowerRight.lng, savedLowerRight.lat],
        [savedUpperLeft.lng, savedLowerRight.lat],
        [savedUpperLeft.lng, savedUpperLeft.lat]
    ];

    map.addLayer({
        'id': 'rectangle',
        'type': 'line',
        'source': {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'geometry': {
                    'type': 'Polygon',
                    'coordinates': [rectangle]
                }
            }
        },
        'layout': {},
        'paint': {
            'line-color': '#088',
            'line-width': 4
        }
    });
}

function addMarker(lng, lat, style) {
    var el = document.createElement('div');
    el.className = style;
    new mapboxgl.Marker(el)
        .setLngLat({lng: lng, lat: lat})
        .addTo(map);
}