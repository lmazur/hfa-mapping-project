﻿var router = require('express').Router();

var Controllers = {
    app: require('./controllers/app'),
}

router.route('/').get(Controllers.app.main);
router.route('/query').post(Controllers.app.query);

module.exports = router;