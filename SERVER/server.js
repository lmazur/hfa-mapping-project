var http = require('http');
var port = process.env.port || 8080;

var bodyParser = require('body-parser');
const express = require('express');
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('port', port);
app.use('/', require('./router'));
app.use(express.static(__dirname + '/public'));

app.listen(port);


/*http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end(process.env.port + '\n');
}).listen(port);*/