<?php
ini_set('memory_limit', '2G');
ini_set('max_execution_time', 0);

class EsriDownloader
{
	public $fields = ['OBJECTID', 'Census_Tract_QualifiedZones_Cen', 'Census_Tract_QualifiedZones_Opp', 'Census_Tract_QualifiedZones_O_1', 'Census_Tract_QualifiedZones_O_2', 'Census_Tract_QualifiedZones_O_3',
        'Census_Tract_QualifiedZones_O_4', 'TractLevelDemographics_tr_OBJEC', 'TractLevelDemographics_tr_ID', 'TractLevelDemographics_tr_NAME', 'TractLevelDemographics_tr_ST_AB', 'TractLevelDemographics_tr_NAMEC',
        'TractLevelDemographics_tr_TOTHH', 'TractLevelDemographics_tr_TOTPO', 'TractLevelDemographics_tr_MEDHI', 'TractLevelDemographics_tr_MINOR', 'TractLevelDemographics_tr_RACEB', 'TractLevelDemographics_tr_NHSPW',
        'TractLevelDemographics_tr_NHSPB', 'TractLevelDemographics_tr_NHSPA', 'TractLevelDemographics_tr_NHS_1', 'TractLevelDemographics_tr_NHSPP', 'TractLevelDemographics_tr_NHSPO', 'TractLevelDemographics_tr_NHSPM',
        'TractLevelDemographics_tr_HISPP', 'TractLevelDemographics_tr_NONHI', 'TractLevelDemographics_tr_EMP_C', 'TractLevelDemographics_tr_UNEMP', 'TractLevelDemographics_tr_N01_E', 'TractLevelDemographics_tr_N01_B',
        'PovertyData_tr_ID', 'PovertyData_tr_ACSHHBPOV', 'PovertyData_tr_ACSTOTHH', 'Tract_Status', 'geom'];
	
	public $textFields = ['Census_Tract_QualifiedZones_Cen', 'Census_Tract_QualifiedZones_Opp', 'Census_Tract_QualifiedZones_O_1', 'Census_Tract_QualifiedZones_O_2', 'Census_Tract_QualifiedZones_O_3',
        'Census_Tract_QualifiedZones_O_4', 'TractLevelDemographics_tr_ID', 'TractLevelDemographics_tr_NAME', 'TractLevelDemographics_tr_ST_AB', 'TractLevelDemographics_tr_NAMEC', 'PovertyData_tr_ID', 'Tract_Status'];

    function __construct() {
		$this->conn = @pg_connect("host=localhost dbname=osm user=postgres password=postgres");
    }

	static function getJSON($offset, $count) {

        $fields = urlencode("OBJECTID,Census_Tract_QualifiedZones_Cen,Census_Tract_QualifiedZones_Opp,Census_Tract_QualifiedZones_O_1,Census_Tract_QualifiedZones_O_2,Census_Tract_QualifiedZones_O_3,Census_Tract_QualifiedZones_O_4,TractLevelDemographics_tr_OBJEC,TractLevelDemographics_tr_ID,TractLevelDemographics_tr_NAME,TractLevelDemographics_tr_ST_AB,TractLevelDemographics_tr_NAMEC,TractLevelDemographics_tr_TOTHH,TractLevelDemographics_tr_TOTPO,TractLevelDemographics_tr_MEDHI,TractLevelDemographics_tr_MINOR,TractLevelDemographics_tr_RACEB,TractLevelDemographics_tr_NHSPW,TractLevelDemographics_tr_NHSPB,TractLevelDemographics_tr_NHSPA,TractLevelDemographics_tr_NHS_1,TractLevelDemographics_tr_NHSPP,TractLevelDemographics_tr_NHSPO,TractLevelDemographics_tr_NHSPM,TractLevelDemographics_tr_HISPP,TractLevelDemographics_tr_NONHI,TractLevelDemographics_tr_EMP_C,TractLevelDemographics_tr_UNEMP,TractLevelDemographics_tr_N01_E,TractLevelDemographics_tr_N01_B,PovertyData_tr_ID,PovertyData_tr_ACSHHBPOV,PovertyData_tr_ACSTOTHH,Tract_Status");
        
		$where = urlencode("Tract_Status!='Unqualified'");
        
		$orderBy = urlencode("OBJECTID ASC");

        $url = "https://services.arcgis.com/AgwDJMQH12AGieWa/ArcGIS/rest/services/Census_Tracts_Qualified_Opportunity_Zones/FeatureServer/0/query?f=json&where={$where}&outFields={$fields}&outSR=4326&returnGeometry=true&resultOffset={$offset}&resultRecordCount={$count}&orderByFields={$orderBy}";

		$ch = curl_init();

		if (curl_errno($ch)) {
			print_r(curl_error($ch), true);
			exit;
		}

		$options = array(
			CURLOPT_URL => $url,
			CURLOPT_CONNECTTIMEOUT => 15,
			CURLOPT_HEADER => FALSE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_SSL_VERIFYHOST => FALSE,
			CURLOPT_SSL_VERIFYPEER => FALSE			
		);
		
		curl_setopt_array($ch, $options);

		$response = curl_exec($ch);
		
		$json = null;

		if ($response === false) {
			echo "Curl error: " . curl_error($ch);			
		} else {
			$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($http_code === 200) {
				$json = json_decode($response);
			}
		}

		curl_close($ch);
		
		return $json;	
	}
	
	static function getPolygonWKT($rings) {
		
		$polygon = "ST_GeomFromText('POLYGON((";
		
		$coordinates = [];
		
		foreach ($rings as $ring) {			
			$coordinates[] = $ring[0] . ' ' . $ring[1];
		}
		
		$polygon .= join(',', $coordinates);
		
		$polygon .= "))', 4326)";
		
		return $polygon;
	}

    static function getMultiPolygonWKT($polygons) {

        $textMultiPolygon = "ST_GeomFromText('MULTIPOLYGON(";

        $textPolygons = [];

        foreach ($polygons as $polygon) {

            $textPolygon = array_reduce($polygon, function ($carry, $item) {
                return $carry .= $item[0] . ' ' . $item[1] . ',';
            }, '');

            $textPolygons[] = '((' . substr_replace($textPolygon, "", -1) . '))';
        }

        $textMultiPolygon .= join(',', $textPolygons);

        $textMultiPolygon .= ")', 4326)";

        return $textMultiPolygon;
    }

	function createZones($values) {
		
        if ($this->conn) {
		
			pg_query($this->conn, "INSERT INTO opportunity_zone(" . join(",", $this->fields) . ") VALUES(" . join(",", $values) . ")");

        } else {

            echo "Error: no database connection", "\n";
        }
    }
	
	function populateFields($feature) {

		$values = [];
		
		foreach ($this->fields as $field) {
			if ($field == 'geom') {
				#$values[] = self::getPolygonWKT($feature->geometry->rings[0]);
                $values[] = self::getMultiPolygonWKT($feature->geometry->rings);
			} else if (in_array($field, $this->textFields)) {
				$values[] = "'" . pg_escape_string($feature->attributes->{$field}) . "'";
			} else {
				$values[] = $feature->attributes->{$field} ? $feature->attributes->{$field} : 'NULL';
			}			
		}

		return $values;
	}
	
	function process() {

		$offset = 0;
		$count = 2000;

        while (true) {

            echo "offset: ", $offset, "\n";

            $json = self::getJSON($offset, $count);

            if ($json && isset($json->features)) {

                foreach ($json->features as $feature) {

                    $values = $this->populateFields($feature);

                    $this->createZones($values);
                }

            } else {

                echo "no features", "\n";

                break;
            }
			
			if (!isset($json->exceededTransferLimit) || $json->exceededTransferLimit === FALSE) {
				
				echo "exceededTransferLimit is false", "\n";
				
				break;
			}			

            $offset += $count;
        }
	}
}

$zip = new EsriDownloader();
$zip->process();